﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageC : ContentPage
    {
        string[] srcs = { "a.png", "b.png", "c.png" };
        private string src = "c.png";
        public string Src
        {
            get
            {
                return src;
            }
            set
            {
                src = value;
                OnPropertyChanged("Src");
            }
        }
        public PageC ()
		{
            BindingContext = this;
			InitializeComponent ();
		}

        void OnTapC(object sender, EventArgs args)
        {
            Random random = new Random();
            int selected = random.Next(0, 3);
            while (srcs[selected] == src)
            {
                selected = random.Next(0, 3);
            }
            Src = srcs[selected];

        }

        void HandleAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page C has appeared.");
        }

        void HandleDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page C has disappeared.");
        }
    }
}