﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageA : ContentPage
    {
        private bool visible = false;
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                OnPropertyChanged("Visible");
            }
        }
        public PageA ()
        {
            BindingContext = this;
            InitializeComponent ();
            Visible = false;
		}

        void OnTapSecret(object sender, EventArgs args)
        {
            Visible = true;
        }

        async void OnButtonSecret(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PageSecret());
        }

        void HandleAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page A has appeared.");
        }

        void HandleDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page A has disappeared.");
        }
    }
}