﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW3_Hierarchy
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        async void OnButton1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PageA());
        }

        async void OnButton2(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PageB());
        }

        async void OnButton3(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PageC());
        }

        void HandleAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("MainPage has appeared.");
        }

        void HandleDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("MainPage has disappeared.");
        }
    }
}
