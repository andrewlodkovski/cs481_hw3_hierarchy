﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageSecret : ContentPage
	{
		public PageSecret ()
		{
			InitializeComponent ();
		}

        async void OnButtonReturn(object sender, EventArgs args)
        {
            await Navigation.PopToRootAsync();
        }

        void HandleAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page Secret has appeared.");
        }

        void HandleDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page Secret has disappeared.");
        }
    }
}