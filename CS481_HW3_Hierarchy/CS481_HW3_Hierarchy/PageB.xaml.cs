﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageB : ContentPage
    {
        public PageB ()
		{
			InitializeComponent ();
		}

        void OnTapB(object sender, EventArgs args)
        {
            Random random = new Random();
            BackgroundColor = new Color(random.NextDouble(), random.NextDouble(), random.NextDouble());
        }

        void HandleAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page B has appeared.");
        }

        void HandleDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page B has disappeared.");
        }
    }
}